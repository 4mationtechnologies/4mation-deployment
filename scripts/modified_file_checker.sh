#!/bin/bash

#This is a simple script that will check your repo for modified files and email 4mation to notify us of a potential hack on the site.
#Details on setting up are found in the readme.md file of this repo.

#get config options
HUB_DIR=$(pwd)
DEPLOYMENT_DIR=$HUB_DIR/4m-deploy/deployment_scripts
source $DEPLOYMENT_DIR/deployment.conf

cd $LIVESITEDIR

MESSAGE=$(git status) #current git status

if ! [[ "$MESSAGE" =~ "nothing to commit" ]]; then #message contains something it shouldn't. Modified or untracked files.

    #Send error email
    SUBJECT="Potential client site hacked - $COMPANYNAME"
    EMAILMESSAGE="/tmp/$COMPANYALIAS-giterror.txt"
    echo "$MESSAGE" > $EMAILMESSAGE
    mail -r "$COMPANYNAME <$COMPANYEMAIL>" -s "$SUBJECT" $SENDERRORTO < $EMAILMESSAGE
    exit 1

fi