# 4mation Deployment Procedure

* Developer: 4mation Technologies
* Author: Dan Churchill <dan@4mation.com.au>
* Contributors: Zak Henry <zak@4mation.com.au> and Dave Clark <dave.clark@4mation.com.au>

***

## Prerequisites

Before setting up the 4m-deploy repo you need to complete the steps in http://confluence.4mation.com.au/display/PD/Deployment+Procedures

## Let's get this party started!

*On the server*

1.  Clone this repo as a non-logged in 4mation user into ~/git/hub.git/4m-deploy with ``` git clone git@bitbucket.org:4mationtechnologies/4mation-deployment.git 4m-deploy```
2.  Back up your hub.git/hooks folder, in case you have anything special in here that you need to preserve since this is currently not git tracked
3.  In hub.git delete the hooks folder ``` rm -r hooks ``` and create a new symlink to our 4m-deploy boilerplate hooks folder ``` ln -s 4m-deploy/hooks ```
4.  In the 4m-deploy repo on the command line execute ``` git config core.fileMode false ```
5.  Ensure that the entire 4m-deploy repo has the correct permissions for the git user to be able to execute ``` chmod -R 755 4m-deploy/ ```

These changes need to be made in your client repo and committed so they're tracked

*Locally on your client repo*

1.  Clone the 4m-deploy repo locally (just so we can get the sample files from it) and copy ``` deployment_scripts.sample ``` folder into your sites publicly accessible webroot. Remove the .sample extensions from the folder and files and commit these to git
2.  Add ``` repo-info.json ``` to your sites .gitignore file since it's an auto-generated file from the 4m-deploy script. This file will sit in your sites deployment_scripts folder
3.  Populate the files environments.csv and deployment.config appropriately and commit these to git
4.  Note: Make sure to add a newline at the end of the csv file in environments.csv
5.  Ensure that the deployments_scripts folder in your client repo has the same 755 permissions as the 4m-deploy repo

#After committing these changes to git and pushing up to the server

*On the server*

1.  In hub.git/4m-deploy run ``ln -s ~/path/to/deployment_scripts/`` which is the path to your git commited client deployment_scripts folder

## Important notes

1.  Never update anything inside the 4m-deploy repo apart from the instructions in this file
2.  Ensure that the git user has permissions to the hooks folder
3.  The first push up with your new deployment_scripts folder will need to be manually done
4.  Ensure that any git repos on the server including hub do not have any references to Stash
5.  When connecting your site to the hub on the server, it needs to be referred to as hub for the deployment process to work properly
6.  Whilst you can symlink your deployment_scripts folder on the live site I strongly reccomend you do it from the staging site only. If the process falls over it can continue execution and you don't want to mess up the live site.

Environments.csv is your client specific deployment strategies. Each row is a deployment strategy and the system only matches the first one found. Column definitions are:

env: This is just an environment name, stored in a variable (TBA) that will be usable in your client deploy.sh script.
hostname: The hostname to match on for deployments. ie squadron.anchor.net.au, castaway, or your local instance.
strategy: This is the branch regex to match against for a pushed branch. This can be exact branch matches ie "master" or partial wildcard matches such as "release-*"
directory: This is the directory to deploy into for this strategy, do not include a ~ sign in the path. Absolute paths only.


### Checking if your client site has been hacked

Sometimes when a client site gets hacked there are modified files where code has been injected. We typically only get notified of these hacks when the client notices
or someone informs the client that there is an issue. Some of these hacks are subtle and won't get picked up as easily.

Rather than being *reactive* to these issues, let's be *proactive* and have a script that checks for modified files on the server and emails us if there are any.

This script will:

1. Run every 10 minutes
2. Check git to see if there are *any* modified (or untracked) files in your repo
3. Email all.staff+hack@4mation.com.au so it can be looked at.

In order to run this script you need to update your deployment.conf file LIVESITEDIR directive. Run the following commands on the server so you set the cron to run.

``crontab -e``
``*/10 * * * * /bin/sh /home/client/git/hub.git/4m-deploy/scripts/modified_file_checker.sh``

And you're done!
